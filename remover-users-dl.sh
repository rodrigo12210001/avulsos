#!/bin/bash

#Para filtrar todas as contas com estado bloqueado no server
usuarios_bloqueados=$(zmprov sa zimbraAccountStatus=locked)

#Para procurar as listas de distribuição que o usuário bloqueado está inserido
for usuario in $usuarios_bloqueados ; do
	lists_user=$(zmsoap -z GetAccountMembershipRequest/account=$usuario @by=name | grep -o '\b\w*@unimedjp.com.br\w*\b')
	#Para remover cada usuário filtrado da lista de distribuição que ele faz parte
	for lista in $lists_user ; do
		zmprov rdlm $lista $usuario
		echo "Removido $usuario de $lista"
	done
done
echo "Done, bye"