#!/bin/bash


#obtenha a lista de usuários bloqueados
users=$(zimbra user list -b | awk '{print $1}')

# Para cada usuário bloqueado, remova-o de todos os grupos
for user in $users; do
  zimbra group removeuser $user *
  done

